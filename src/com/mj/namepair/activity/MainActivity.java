package com.mj.namepair.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.namepair.R;
import com.mj.namepair.service.NamepairService;
import com.mj.namepair.util.NetUtils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 姓名配对测试
 * @author zhaominglei
 * @date 2015-4-7
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	private NamepairService namepairService = new NamepairService();
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private EditText boynameText; //男生姓名输入框
	private String boyname; //男生姓名
	private EditText girlnameText; //女生姓名输入框
	private String girlname; //女生姓名
	private Button queryBtn; //配对测试
	private ProgressBar queryProgressBar; //查询进度条
	private TextView resultView; //结果
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		boynameText = (EditText)findViewById(R.id.namepair_boyname_edt);
		girlnameText = (EditText)findViewById(R.id.namepair_girlname_edt);
		queryBtn = (Button)findViewById(R.id.namepair_query_btn);
		queryProgressBar = (ProgressBar)findViewById(R.id.namepair_query_progressbar);
		resultView = (TextView)findViewById(R.id.namepair_result);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		queryBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, "63fe393625ef99b9FExXHlFYt8lF4WTlBbNLZRSf+pQKYaAMQ2t8E3dYvQDgqPh1mw", "木蚂蚁");
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, "63fe393625ef99b9FExXHlFYt8lF4WTlBbNLZRSf+pQKYaAMQ2t8E3dYvQDgqPh1mw", "木蚂蚁");
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.namepair_query_btn:
			boyname = boynameText.getText().toString();
			if (boyname == null || boyname.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.namepair_boyname_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			girlname = girlnameText.getText().toString();
			if (girlname == null || girlname.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.namepair_girlname_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			resultView.setText("");
			
		    getNamepairInfo();		
			break;
			
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;

		default:
			break;
		}
	}
	
	private void getNamepairInfo() {
		if (NetUtils.isConnected(getApplicationContext())) {
			queryProgressBar.setVisibility(View.VISIBLE);
			new NamepairInfoAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
		}
	}	

	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	public class NamepairInfoAsyncTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			return namepairService.getNamepairInfo(boyname, girlname);
		}

		@Override
		protected void onPostExecute(String result) {
			queryProgressBar.setVisibility(View.GONE);
			if (result != null && !result.equals("")) {
				resultView.setText(Html.fromHtml(result));
			} else {
				Toast.makeText(getApplicationContext(), R.string.namepair_error1, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
