package com.mj.namepair.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mj.namepair.util.HttpUtils;

/**
 * 姓名配对测试
 * @author zhaominglei
 * @date 2015-4-7
 * 
 */
public class NamepairService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = NamepairService.class.getSimpleName();
	private static final String M_SHEUP_XINGMING_PEIDUI_URL = "http://m.sheup.com/xingming_peidui_1.php"; //三藏算命网手机版
	
	public String getNamepairInfo(String boyname, String girlname) {
		if (boyname == null || boyname.equals("") 
				|| girlname == null || girlname.equals("")) {
			return null;
		}
		String html = pair(boyname, girlname);
		if (html == null || html.equals("")) {
			return null;
		}
		Document document = Jsoup.parse(html);
		Elements elements = document.getElementsByClass("sanzang_subs");
		if (elements != null && !elements.isEmpty()) {
			StringBuilder result = new StringBuilder();
			int i = 0;
			for (Element element : elements) {
				String content = element.html();
				if (i == 0) {
					result.append(content);
					break;
				}
				i++;
			}
			return result.toString();
		}
		return null;
	}
	
	public String pair(String boyname, String girlname) {
		StringBuilder param = new StringBuilder();
		param.append("xingx=").append(HttpUtils.encodeURI(boyname, "gb2312"));
		param.append("&mingx=").append(HttpUtils.encodeURI(girlname, "gb2312"));
		param.append("&xingming=%D0%D5%C3%FB%C5%E4%B6%D4%B2%E2%CA%D4");
		String html = HttpUtils.doPostForMSheup(M_SHEUP_XINGMING_PEIDUI_URL, param.toString(), "gbk");
		return html;
	}
}
